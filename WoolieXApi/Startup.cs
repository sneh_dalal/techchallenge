using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WoolieXApi.Services;
using WoolieXApi.Services.Configuration;
using WoolieXApi.Worker;

namespace WoolieXApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddScoped<IExerciseApiConfig, ExerciseApiConfig>();            
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IShopperHistoryService, ShopperHistoryService>();
            services.AddScoped<ITrolleyService, TrolleyService>();            
            services.AddScoped<ISortProductsWorker, SortByRelevanceWorker>();
            services.AddScoped<IResultProcessorWorker, ResultProcessorWorker>();            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");                
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "api",
                    defaults: new { controller = "User" });
            });
        }
    }
}
