﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using WoolieXApi.Exceptions;

namespace WoolieXApi.Worker
{
    internal class ResultProcessorWorker : IResultProcessorWorker
    {
        public async Task<HttpResponseMessage> ProcessRequestAsync(HttpRequestMessage request)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.SendAsync(request);

                    if (!response.IsSuccessStatusCode)
                    {
                        await ProcessErrorResponseAsync(response);
                    }

                    return response;
                }
                catch (WebException ex)
                {
                    throw new ApiUnavailableException("Services are currently unavailable", ex);
                }                
            }
        }

        private async Task ProcessErrorResponseAsync(HttpResponseMessage response)
        {
            string error = await response.Content.ReadAsStringAsync();

            switch (response.StatusCode)
            {
                case HttpStatusCode.BadRequest:
                    {
                        if (!string.IsNullOrEmpty(error))
                        {
                            throw new BadRequestException(error);
                        }
                        else
                        {
                            throw new BadRequestException();
                        }
                    }

                case HttpStatusCode.Unauthorized:
                    {
                        if (!string.IsNullOrEmpty(error))
                        {
                            throw new NotAuthorisedException(error);
                        }
                        else
                        {
                            throw new NotAuthorisedException();
                        }
                    }

                case HttpStatusCode.NotFound:
                    {
                        throw new NotFoundException("Api not found");
                    }

                default:
                    {
                        throw new UnexpectedException(string.Format("Api Request Failed", response.StatusCode, response.ReasonPhrase, response.Headers));
                    }
            }
        }
    }
}
