﻿using System.Net.Http;
using System.Threading.Tasks;

namespace WoolieXApi.Worker
{
    public interface IResultProcessorWorker
    {
        Task<HttpResponseMessage> ProcessRequestAsync(HttpRequestMessage request);
    }
}
