﻿using System.Collections.Generic;
using WoolieXApi.Dto;

namespace WoolieXApi.Worker
{
    public interface ISortProductsWorker
    {
        List<ProductDto> GetSortedProductsList(List<ProductDto>products, List<CustomerOrderDto> shoppedHistory);
    }
}
