﻿using System.Collections.Generic;
using System.Linq;
using WoolieXApi.Dto;

namespace WoolieXApi.Worker
{
    public class SortByRelevanceWorker : ISortProductsWorker
    {
        public List<ProductDto> GetSortedProductsList(List<ProductDto> products, List<CustomerOrderDto> shopperHistory)
        {
            if(shopperHistory != null)
            {
                var customerOrderList = shopperHistory
                                .SelectMany(customerOrders =>
                                    customerOrders.Products.Select(product => new ProductDto()
                                    {
                                        Name = product.Name,
                                        Price = product.Price,
                                        Quantity = product.Quantity
                                    })).ToList();

                //sort the shopper history based on quantity
                var sortedShopperListByRelevance = (from customerOrders in customerOrderList
                                                    group customerOrders by (customerOrders.Name, customerOrders.Price) into groupedCustomerOrder
                                                    select new
                                                    {
                                                        groupedCustomerOrder.Key.Name,                                                        
                                                        Quantity = groupedCustomerOrder.Sum(a => a.Quantity)
                                                    }).OrderByDescending(x => x.Quantity).ToList();

                //get the actual quantity and price value from products 
                var intrResult1 = from sslr in sortedShopperListByRelevance
                             join prod in products on sslr.Name equals prod.Name into sortedList
                             from sl in sortedList.DefaultIfEmpty()
                             select new ProductDto
                             {
                                 Name = sl.Name,
                                 Price = sl.Price,
                                 Quantity = sl.Quantity
                             };
                //get products which are not there in the shopper history and union them with sorted shopper history list
                var intrResult2 = from prod in products
                            where sortedShopperListByRelevance.All(ss => !ss.Name.Equals(prod.Name, System.StringComparison.OrdinalIgnoreCase))
                            select new ProductDto
                            {
                                Name = prod.Name,
                                Price = prod.Price,
                                Quantity = prod.Quantity
                            };                

               return (intrResult1.Union(intrResult2)).ToList();
            }

            return products;            
        }
    }
}