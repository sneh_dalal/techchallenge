﻿using System.Collections.Generic;
using System.Linq;
using WoolieXApi.Dto;

namespace WoolieXApi.Helpers
{
    public static class ProductsSortHelper
    {
        public static List<ProductDto> SortProducts(this List<ProductDto> products, string sortOption)
        {
            switch(sortOption.ToLower())
            {
                case "low":
                        return products.OrderBy(x => x.Price).ToList();                    
                case "high":
                    return products.OrderByDescending(x => x.Price).ToList();
                case "ascending":
                    return products.OrderBy(x => x.Name).ToList();
                case "descending":
                    return products.OrderByDescending(x => x.Name).ToList();                
                default:
                    return products;
            }            
        }        
    }
}