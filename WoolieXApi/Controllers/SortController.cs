﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WoolieXApi.Dto;
using WoolieXApi.Services;

namespace WoolieXApi.Controllers
{
    [ApiController]    
    [Route("api/[controller]")]
    public class SortController : Controller
    {
        private readonly IProductService _productService;

        public SortController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<List<ProductDto>> GetSortedProductsAsync(string sortOption)
        {
            if(!string.IsNullOrWhiteSpace(sortOption))
                return await _productService.GetSortedProductsAsync(sortOption);
            return await _productService.GetProductsAsync();
        }
    }
}