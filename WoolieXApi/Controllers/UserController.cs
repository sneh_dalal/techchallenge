﻿using Microsoft.AspNetCore.Mvc;

namespace WoolieXApi.Controllers
{
    [ApiController]
    [Route("api")]
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        [HttpGet]        
        public JsonResult GetUser()
        {
            return Json(new { name = "test", token = "d259092d-3b4e-4da8-9fed-4a5b4cb7002c" });
        }
    }
}