﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace WoolieXApi.Controllers
{
    // more suited in front end apps to show users a custom error view
    public class ErrorController : Controller
    {
        [Route("Error")]
        public string Error()
        {
            var exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            return exceptionDetails.Error.Message;
        }
    }
}
