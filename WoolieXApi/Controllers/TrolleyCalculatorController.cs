﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WoolieXApi.Dto;
using WoolieXApi.Services;

namespace WoolieXApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TrolleyCalculatorController : Controller
    {
        private readonly ITrolleyService _trolleyService;

        public TrolleyCalculatorController(ITrolleyService trolleyService)
        {
            _trolleyService = trolleyService;
        }

        [HttpPost]
        public async Task<decimal> PostTrolleyLowestPossibleAsync(TrolleyDto trolley)
        {
            return await _trolleyService.GetTrolleyLowestPossibleAsync(trolley);
        }
    }
}
