﻿using System;
namespace WoolieXApi.Exceptions
{
    public class UnexpectedException : Exception
    {
        public UnexpectedException() : base()
        {
        }

        public UnexpectedException(string message) : base(message)
        {

        }
    }
}
