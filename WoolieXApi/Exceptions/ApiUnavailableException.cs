﻿using System;

namespace WoolieXApi.Exceptions
{
    public class ApiUnavailableException : Exception
    {
        public ApiUnavailableException() : base()
        {

        }

        public ApiUnavailableException(string message) : base(message)
        {

        }

        public ApiUnavailableException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}