﻿using System;
namespace WoolieXApi.Exceptions
{
    public class NotAuthorisedException : Exception
    {
        public NotAuthorisedException() : base()
        {
        }

        public NotAuthorisedException(string message) : base(message)
        {
        }
    }
}
