﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WoolieXApi.Dto;

namespace WoolieXApi.Services
{
    public interface IShopperHistoryService
    {
        Task<List<CustomerOrderDto>> GetShopperHistoryAsync();
    }
}
