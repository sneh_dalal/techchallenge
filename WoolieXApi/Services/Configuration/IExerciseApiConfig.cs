﻿namespace WoolieXApi.Services.Configuration
{
    public interface IExerciseApiConfig
    {
        string ServiceUrl { get; }
        string Token { get; }
    }
}
