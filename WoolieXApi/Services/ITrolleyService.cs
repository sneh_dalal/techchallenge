﻿using System.Threading.Tasks;
using WoolieXApi.Dto;

namespace WoolieXApi.Services
{
    public interface ITrolleyService
    {
        Task<decimal> GetTrolleyLowestPossibleAsync(TrolleyDto trolley);
    }
}