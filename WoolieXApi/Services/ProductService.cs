﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WoolieXApi.Helpers;
using WoolieXApi.Dto;
using WoolieXApi.Services.Configuration;
using WoolieXApi.Worker;

namespace WoolieXApi.Services
{
    public class ProductService : IProductService
    {
        private const string PRODUCT_ENDPOINT = @"{0}/products?token={1}";

        private readonly IShopperHistoryService _shopperHistoryService;
        private readonly ISortProductsWorker _sortWorker;
        private readonly IResultProcessorWorker _resultProcessorWorker;

        private static JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        private IExerciseApiConfig _config;

        public ProductService(IExerciseApiConfig config, ISortProductsWorker sortWorker,
            IShopperHistoryService shopperHistoryService, IResultProcessorWorker resultProcessorWorker)
        {
            _config = config;
            _sortWorker = sortWorker;
            _shopperHistoryService = shopperHistoryService;
            _resultProcessorWorker = resultProcessorWorker;
        }

        public async Task<List<ProductDto>> GetSortedProductsAsync(string sortOption)
        {
            var products = await GetProductsAsync();

            if(products != null)
            {
                if (!sortOption.Equals("Recommended", StringComparison.OrdinalIgnoreCase))
                    return products.SortProducts(sortOption);

                var shopperHistory = await _shopperHistoryService.GetShopperHistoryAsync();
                return _sortWorker.GetSortedProductsList(products, shopperHistory);
            }
            
            return null;
        }

        public async Task<List<ProductDto>> GetProductsAsync()
        {
            Uri uri = new Uri(string.Format(PRODUCT_ENDPOINT, _config.ServiceUrl, _config.Token));

            HttpRequestMessage request = new HttpRequestMessage
            {
                RequestUri = uri,
                Method = HttpMethod.Get
            };

            HttpResponseMessage response = await _resultProcessorWorker.ProcessRequestAsync(request);

            string json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<ProductDto>>(json, _jsonSettings);
        }
    }
}
