﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WoolieXApi.Dto;
using WoolieXApi.Services.Configuration;
using WoolieXApi.Worker;

namespace WoolieXApi.Services
{
    public class TrolleyService : ITrolleyService
    {
        private const string PRODUCT_ENDPOINT = @"{0}/trolleyCalculator?token={1}";
        private readonly IResultProcessorWorker _resultProcessorWorker;

        private static JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        private IExerciseApiConfig _config;

        public TrolleyService(IResultProcessorWorker resultProcessorWorker, IExerciseApiConfig exerciseApiConfig)
        {
            _resultProcessorWorker = resultProcessorWorker;
            _config = exerciseApiConfig;
        }

        public async Task<decimal> GetTrolleyLowestPossibleAsync(TrolleyDto trolley)
        {
            Uri uri = new Uri(string.Format(PRODUCT_ENDPOINT, _config.ServiceUrl, _config.Token));

            StringContent content = new StringContent(JsonConvert.SerializeObject(trolley, _jsonSettings), Encoding.UTF8, "application/json");

            HttpRequestMessage request = new HttpRequestMessage
            {
                RequestUri = uri,
                Method = HttpMethod.Post,
                Content = content
            };

            HttpResponseMessage response = await _resultProcessorWorker.ProcessRequestAsync(request);

            string json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<decimal>(json, _jsonSettings);
        }
    }
}
