﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WoolieXApi.Dto;

namespace WoolieXApi.Services
{
    public interface IProductService
    {
        Task<List<ProductDto>> GetProductsAsync();
        Task<List<ProductDto>> GetSortedProductsAsync(string sortOption);
    }
}
