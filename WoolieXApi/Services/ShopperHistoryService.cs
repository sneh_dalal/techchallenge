﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WoolieXApi.Dto;
using WoolieXApi.Services.Configuration;
using WoolieXApi.Worker;

namespace WoolieXApi.Services
{
    public class ShopperHistoryService : IShopperHistoryService
    {
        private const string SHOPPER_HISTORY_ENDPOINT = @"{0}/shopperHistory?token={1}";

        private readonly IResultProcessorWorker _resultProcessorWorker;
        private static JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        private IExerciseApiConfig _config;

        public ShopperHistoryService(IExerciseApiConfig config, IResultProcessorWorker resultProcessorWorker)
        {
            _config = config;
            _resultProcessorWorker = resultProcessorWorker;
        }

        public async Task<List<CustomerOrderDto>> GetShopperHistoryAsync()
        {
            Uri uri = new Uri(string.Format(SHOPPER_HISTORY_ENDPOINT, _config.ServiceUrl, _config.Token));

            HttpRequestMessage request = new HttpRequestMessage
            {
                RequestUri = uri,
                Method = HttpMethod.Get
            };

            HttpResponseMessage response = await _resultProcessorWorker.ProcessRequestAsync(request);

            string json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<CustomerOrderDto>>(json, _jsonSettings);
        }
    }
}
