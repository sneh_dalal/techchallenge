﻿using System.Collections.Generic;

namespace WoolieXApi.Dto
{
    public class SpecialDto
    {
        public List<QuantityDto> Quantities;
        public decimal total;
    }
}
