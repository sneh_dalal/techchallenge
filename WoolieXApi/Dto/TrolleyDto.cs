﻿using System.Collections.Generic;

namespace WoolieXApi.Dto
{
    public class TrolleyDto
    {
        public List<ProductDto> Products;
        public List<SpecialDto> Specials;
        public List<QuantityDto> Quantities;        
    }
}
