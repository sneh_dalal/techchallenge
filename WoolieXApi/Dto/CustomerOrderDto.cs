﻿using System.Collections.Generic;

namespace WoolieXApi.Dto
{
    public class CustomerOrderDto
    {
        public int CustomerId { get; set; }
        public List<ProductDto> Products { get; set; }
    }
}
