﻿namespace WoolieXApi.Dto
{
    public class QuantityDto
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
