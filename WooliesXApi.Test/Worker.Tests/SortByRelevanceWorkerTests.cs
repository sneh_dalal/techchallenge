﻿using System.Collections.Generic;
using FluentAssertions;
using WoolieXApi.Dto;
using WoolieXApi.Worker;
using Xunit;

namespace WooliesXApi.Test.Worker.Tests
{
    public class SortByRelevanceWorkerTests
    {
        private SortByRelevanceWorker _sortByRelevanceWorker;

        public SortByRelevanceWorkerTests()
        {
            _sortByRelevanceWorker = new SortByRelevanceWorker();
        }

        [Fact]
        public void GetSortedProductsList_NullShopperHistory_ReturnsOriginalProductsList()
        {
            var products = GetProductList();

            var result = _sortByRelevanceWorker.GetSortedProductsList(products, null);

            result[0].Name.Should().BeEquivalentTo(products[0].Name);
            result[1].Name.Should().BeEquivalentTo(products[1].Name);
            result[2].Name.Should().BeEquivalentTo(products[2].Name);
            result[3].Name.Should().BeEquivalentTo(products[3].Name);
        }
        
        [Fact]
        public void GetSortedProductsList_NotNullShopperHistory_ReturnsSortedProductListBasedOnRelevance()
        {
            var result = _sortByRelevanceWorker.GetSortedProductsList(GetProductList(), GetShopperHistory());            

            result[0].Name.Should().BeEquivalentTo("D");
            result[1].Name.Should().BeEquivalentTo("C");
            result[2].Name.Should().BeEquivalentTo("A");
            result[3].Name.Should().BeEquivalentTo("B");
        }        

        private List<ProductDto> GetProductList()
        {
            return new List<ProductDto>
            {
                new ProductDto
                {
                    Name = "A",
                    Price = 10.0M,
                    Quantity = 0
                },
                new ProductDto
                {
                    Name = "B",
                    Price = 20.0M,
                    Quantity = 0
                },
                new ProductDto
                {
                    Name = "C",
                    Price = 5.0M,
                    Quantity = 0
                },
                new ProductDto
                {
                    Name = "D",
                    Price = 15.0M,
                    Quantity = 0
                }
            };
        }

        private List<CustomerOrderDto> GetShopperHistory()
        {
            return new List<CustomerOrderDto>
            {
                new CustomerOrderDto
                {
                    CustomerId = 1,
                    Products = new List<ProductDto>
                    {
                        new ProductDto
                        {
                            Name = "C",
                            Quantity = 2,
                            Price = 5.0M
                        },
                        new ProductDto
                        {
                            Name = "D",
                            Quantity = 3,
                            Price = 15.0M
                        }
                    }
                },
                new CustomerOrderDto
                {
                    CustomerId = 2,
                    Products = new List<ProductDto>
                    {
                        new ProductDto
                        {
                            Name = "A",
                            Quantity = 1,
                            Price = 10.0M
                        },
                        new ProductDto
                        {
                            Name = "D",
                            Quantity = 1,
                            Price = 15.0M
                        }
                    }
                }
            };
        }
    }
}
