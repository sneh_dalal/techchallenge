﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using WoolieXApi.Controllers;
using WoolieXApi.Dto;
using WoolieXApi.Services;
using Xunit;

namespace WooliesXApi.Test.Controller.Tests
{
    public class SortControllerTests
    {

        private readonly Mock<IProductService> _mockProductService = new Mock<IProductService>();
        private readonly SortController _sortController;

        public SortControllerTests()
        {
            _mockProductService.Setup(x => x.GetProductsAsync())
                .Returns(Task.FromResult<List<ProductDto>>(new List<ProductDto>()));

            _mockProductService.Setup(x => x.GetSortedProductsAsync(It.IsAny<string>()))
                .Returns(Task.FromResult<List<ProductDto>>(new List<ProductDto>()));

            _sortController = new SortController(_mockProductService.Object);
        }

        [Fact]
        public void GetSortProductsAsync_EmptyStringQuery_ReturnsUnsortedProductList()
        {
            var result = _sortController.GetSortedProductsAsync("");

            _mockProductService.Verify(x => x.GetProductsAsync(), Times.Once);
            _mockProductService.Verify(x => x.GetSortedProductsAsync(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void GetSortProductsAsync_StringQueryNotEmpty_ReturnSortedProductList()
        {
            var result = _sortController.GetSortedProductsAsync("low");

            _mockProductService.Verify(x => x.GetProductsAsync(), Times.Never);
            _mockProductService.Verify(x => x.GetSortedProductsAsync(It.IsAny<string>()), Times.Once);
        }
    }
}
